from django.urls import path
from django.conf import settings
from django.conf.urls import include
from django.conf.urls.static import static
from rest_framework_jwt.views import obtain_jwt_token

from users import views

# Restringir url's

urlpatterns = [
    path('',include(router.urls)),
    path('login/', view=obtain_jwt_token, name='login')
]
