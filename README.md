Para realizar una prueba se requiere instalar las siguientes herramientas usando pip
`pip install -t requirements.txt`
posteriormente, realizar migraciones

`python manage.py makemigrations`
`python manage.py migrate`


y por último crear un usuario

`python manage.py createsuperuser`

Para realizar prueba se debe entrar correr el servidor

`python manage.py runserver`

y entrar a la url 'http://127.0.0.1:8000/login/'. 
